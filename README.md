# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Recoded libc's "printf" function;
* It's prototyped similarly to printf;
* Managed:

    + the following conversions: **sSpdDioOuUxXcC**
    + **%%**
    + the flags **#0-+** and **space**
    + **Minimum field-width**
    + **precision**
    + the flags **hh, h, l, ll, j, z**
    + **asterisk**

### How do I get set up? ###

* Library used: libft.a;
* To run tests compile printf_main.c file with libftprintf.a & libft.a librarise with *-Wno-format-invalid-specifier -Wno-format -Wno-macro-redefined -Wno-implicitly-unsigned-literal*. Run, it will execute similar test for original printf and ft_printf functions.
![Screen Shot 2017-06-30 at 6.59.59 PM.png](https://bitbucket.org/repo/XX544eb/images/304157966-Screen%20Shot%202017-06-30%20at%206.59.59%20PM.png) "example of main_printf.c execution"