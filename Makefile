NAME = libftprintf.a
CC = clang
CFLAGS = -Wall -Wextra -Werror -O3
INCLUDES = -I $(HEADERS_FLD) -I $(LIBFT_HEAD_FLD)

SOURCES_FLD = src_printf
HEADERS_FLD = include_printf
LIBFT_HEAD_FLD = ../libft
OBJECTS_FLD = obj_printf

OBJECTS =\
	$(OBJECTS_FLD)/ft_con.o\
	$(OBJECTS_FLD)/ft_con_sc.o\
	$(OBJECTS_FLD)/ft_convers.o\
	$(OBJECTS_FLD)/ft_param_make.o\
	$(OBJECTS_FLD)/ft_precis.o\
	$(OBJECTS_FLD)/ft_printf.o


HEADERS =\
	$(HEADERS_FLD)/printf.h

all : $(NAME)

include Libft_src

$(NAME) : $(OBJECTS) $(HEADERS) $(LIBFTLS_SRC)
	ar rc $(NAME) $(OBJECTS)
	ranlib $(NAME)

.IGNORE : clean fclean

clean :
	rm -rf $(OBJECTS_FLD)

fclean : clean
	rm -f $(NAME)

re : fclean all

$(OBJECTS_FLD)/ft_con.o : $(SOURCES_FLD)/ft_con.c $(HEADERS)
	mkdir -p $(OBJECTS_FLD)
	$(CC) $(INCLUDES) $(CFLAGS) -c $(SOURCES_FLD)/ft_con.c -o $(OBJECTS_FLD)/ft_con.o

$(OBJECTS_FLD)/ft_con_sc.o : $(SOURCES_FLD)/ft_con_sc.c $(HEADERS)
	$(CC) $(INCLUDES) $(CFLAGS) -c $(SOURCES_FLD)/ft_con_sc.c -o $(OBJECTS_FLD)/ft_con_sc.o

$(OBJECTS_FLD)/ft_convers.o : $(SOURCES_FLD)/ft_convers.c $(HEADERS)
	$(CC) $(INCLUDES) $(CFLAGS) -c $(SOURCES_FLD)/ft_convers.c -o $(OBJECTS_FLD)/ft_convers.o

$(OBJECTS_FLD)/ft_param_make.o : $(SOURCES_FLD)/ft_param_make.c $(HEADERS)
	$(CC) $(INCLUDES) $(CFLAGS) -c $(SOURCES_FLD)/ft_param_make.c -o $(OBJECTS_FLD)/ft_param_make.o

$(OBJECTS_FLD)/ft_precis.o : $(SOURCES_FLD)/ft_precis.c $(HEADERS)
	$(CC) $(INCLUDES) $(CFLAGS) -c $(SOURCES_FLD)/ft_precis.c -o $(OBJECTS_FLD)/ft_precis.o

$(OBJECTS_FLD)/ft_printf.o : $(SOURCES_FLD)/ft_printf.c $(HEADERS)
	$(CC) $(INCLUDES) $(CFLAGS) -c $(SOURCES_FLD)/ft_printf.c -o $(OBJECTS_FLD)/ft_printf.o
